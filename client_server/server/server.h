#ifndef SERVER_H
#define SERVER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QVector>
#include <QDataStream>
#include <QTime>
//DB
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QtSql/QtSql>

enum TypeOfBroadcast
{
    PRIVATE,
    BROADCAST,
    BROADCAST_IGNORE_SENDER
};

class Server : public QTcpServer{
    Q_OBJECT

public:
    Server();
    ~Server();
    QTcpSocket *socket;
    bool connectDB();
    bool authorizeUser(QTcpSocket* socket);
    bool registerUser(QTcpSocket* socket);

private:
    QVector <QTcpSocket*> Sockets;
    QMap <qintptr, QTcpSocket*> desriptorToSocket;
    QMap <QTcpSocket*, qintptr> socketToDesriptor;
    QMap <QTcpSocket*, QString> userLogin;
    QMap <QTcpSocket*, QString> userPassword;
    QByteArray Data;
    void SendToClient(QString str, QTcpSocket* sender, TypeOfBroadcast typeBroadcast, quint16 messageType);
    void SendToClient(bool flag, QTcpSocket* sender, TypeOfBroadcast typeBroadcast, quint16 messageType);
    quint16 nextBlockSize;

    //BD
    QSqlQuery *query;


    QString db_input;
    QSqlDatabase mw_db;
    int _userCount = 0;
public slots:
    void incomingConnection(qintptr socketDesriptor);
    void slotReadyRead();
    void slotRemoveUser();
};

#endif // SERVER_H
