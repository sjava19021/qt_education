#include "server.h"
constexpr quint16 MessageTypeLogin = quint16(0);
constexpr quint16 MessageTypePassword = quint16(1);
constexpr quint16 MessageTypePlainText = quint16(2);
constexpr quint16 MessageTypeIsAuthDone = quint16(3);
constexpr quint16 MessageTypeRegisterUser = quint16(4);
constexpr quint16 MessageTypeAddUser = quint16(5);
constexpr quint16 MessageTypeRemoveUser = quint16(6);
constexpr quint16 MessageTypePrivateMessage = quint16(7);


Server::Server(){
    if(this->listen(QHostAddress::Any, 2323)){
        qDebug() << "server started";
    }
    else{
        qDebug() << "server error";
    }
    nextBlockSize = 0;

    if(!connectDB())
    {
        qDebug() << "Failed to connect DB";
    }
    QSqlQuery query;

    db_input = "CREATE TABLE userlist ( "
               "number INTEGER PRIMARY KEY NOT NULL,"
               "name VARCHAR(30), "
               "pass VARCHAR(30)); ";
    if(!query.exec(db_input)){
        qDebug() << "Unable to create a table" << query.lastError();
    }
}

bool Server::authorizeUser(QTcpSocket* socket){
    QString _userName = userLogin[socket];
    QString _userPass = userPassword[socket];

    QString str_t = " SELECT * "
                    " FROM userlist "
                    " WHERE name = '%1'";
    // int db_number = 0;

    QString username = "";
    QString userpass = "";

    db_input = str_t.arg(_userName);

    QSqlQuery query;
    QSqlRecord rec;

    if(!query.exec(db_input)){
        qDebug() << "Unable to execute query - exiting" << query.lastError() << " : " << query.lastQuery();
    }
    rec = query.record();
    query.next();
    _userCount = query.value(rec.indexOf("number")).toInt();
    username = query.value(rec.indexOf("name")).toString();
    userpass = query.value(rec.indexOf("pass")).toString();
    if(_userName != username || _userPass != userpass)
    {
        qDebug() << "Password missmatch" << username << " " << userpass;
        return false;
    }
    else
    {
        return true;
    }
}

Server::~Server(){
    mw_db.removeDatabase("authorisation");
}

void Server::incomingConnection(qintptr socketDescriptor){
    socket = new QTcpSocket;
    socket->setSocketDescriptor(socketDescriptor);
    connect(socket, &QTcpSocket::readyRead, this, &Server::slotReadyRead);
    connect(socket, &QTcpSocket::disconnected, socket, &QTcpSocket::deleteLater);
    connect(socket, &QTcpSocket::disconnected, this, &Server::slotRemoveUser);

    Sockets.push_back(socket);
    desriptorToSocket.insert(socketDescriptor, socket);
    socketToDesriptor.insert(socket, socketDescriptor);
}

void Server::slotRemoveUser(){
    QTcpSocket* socket = (QTcpSocket *) sender();
    QString login = userLogin[socket];
    QString broadcastInfo = "-->DISCONNECTED FROM SERVER!<--";
    qDebug() << broadcastInfo;
    SendToClient(broadcastInfo, socket, TypeOfBroadcast::BROADCAST_IGNORE_SENDER, MessageTypePlainText);
    //SendToClient(login, socket, TypeOfBroadcast::BROADCAST_IGNORE_SENDER, MessageTypeRemoveUser);
    qintptr socketDescriptor = socketToDesriptor[socket];
    Sockets.removeOne(socket);
    desriptorToSocket.remove(socketDescriptor);
    socketToDesriptor.remove(socket);
    userLogin.remove(socket);
    userPassword.remove(socket);
    socket->close();
}

void Server::slotReadyRead(){
    socket = (QTcpSocket*)sender();
    socket->waitForBytesWritten();
    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_DefaultCompiledVersion);
    if(in.status() == QDataStream::Ok){
        for(;;){
            if(nextBlockSize == 0){
                if(socket->bytesAvailable() < 2)
                    break;

                in >> nextBlockSize;
                qDebug() << "nextBlockSize = " << nextBlockSize;
            }
            if(socket->bytesAvailable() < nextBlockSize){
                qDebug() << "Data not full, break";
                break;
            }
            quint16 messageType;
            QString str;
            QTime time;
            bool isDone = false;
            in >> messageType >> time;
            qDebug() << messageType << time;
            switch (messageType) {
                case MessageTypeLogin:
                    in >> str;
                    qDebug() << "0: " << str;
                    userLogin.insert(socket, str);
                    break;

                case MessageTypePassword:
                    in >> str;
                    qDebug() << "1: " << str;
                    userPassword.insert(socket, str);
                    break;

                case MessageTypePlainText:
                    in >> str;

                    if(str.contains("PRIVATE")){
                        QString loginTo;
                        int i = 0;
                        while(i < loginTo.length()){
                            if(str[i] == '_')
                                break;
                            i++;
                        }
                        i++;
                        while(i < str.size() && str[i] != ':'){
                            loginTo.append(str[i]);
                        }
                        QTcpSocket* socketTo;
                        for(auto it=userLogin.begin(); it!=userLogin.end(); it++)
                        {
                          if(it.value()==loginTo)
                          {
                              //нашлось
                              socketTo = it.key();
                              break;
                          }
                        }
                        SendToClient(str + "[from " + userLogin[socket] + " ]",
                                     socketTo, TypeOfBroadcast::BROADCAST, MessageTypePlainText);
                    }else{
                        SendToClient(str, socket, TypeOfBroadcast::BROADCAST, MessageTypePlainText);
                    }
                    qDebug() << "2: " << str;

                    break;

                case MessageTypeIsAuthDone:
                    in >> isDone;
                    isDone = authorizeUser(socket);
                    qDebug() << "3: " << isDone;

                    SendToClient(isDone, socket, TypeOfBroadcast::PRIVATE, MessageTypeIsAuthDone);
                    if(isDone){
                        QString broadcastInfo = "-->CONNECTED TO SERVER!<--";
                        SendToClient(broadcastInfo, socket, TypeOfBroadcast::BROADCAST, MessageTypePlainText);
                        //SendToClient(userLogin[socket], socket, TypeOfBroadcast::BROADCAST, MessageTypeAddUser);
                    }
                    break;
                case MessageTypeRegisterUser:
                    in >> isDone;
                    isDone = registerUser(socket);
                    SendToClient(isDone, socket, TypeOfBroadcast::PRIVATE, MessageTypeRegisterUser);
                    break;
                case MessageTypeRemoveUser:
                    break;
                case MessageTypePrivateMessage:

                default:
                   break;
            }
            nextBlockSize = 0;
        }
    }
    else{
        qDebug() << "DataStream error";
    }
}

void Server::SendToClient(QString str, QTcpSocket* sender, TypeOfBroadcast typeBroadcast, quint16 messageType){
    Data.clear();
    QDataStream out(&Data, QIODevice::WriteOnly);
    QString loginSender = userLogin[sender];
    out.setVersion(QDataStream::Qt_DefaultCompiledVersion);

    switch(messageType){
        case MessageTypeLogin:
            break;

        case MessageTypePassword:
            break;

        case MessageTypePlainText:
            out << quint16(0) << messageType << loginSender << QTime::currentTime() << str;
            break;

        case MessageTypeIsAuthDone:
            out << quint16(0) << messageType << QTime::currentTime() << str;
            break;

        case MessageTypeRegisterUser:
            out << quint16(0) << messageType << QTime::currentTime() << str;
            break;
        case MessageTypeRemoveUser:
            out << quint16(0) << messageType << loginSender << QTime::currentTime();
            break;
        case MessageTypeAddUser:
            out << quint16(0) << messageType << loginSender << QTime::currentTime();
            break;
    }
    out.device() -> seek(0);
    out << quint16(Data.size() - sizeof(quint16));

    switch(typeBroadcast){
        case PRIVATE:
            socket->write(Data);
            socket->waitForBytesWritten();
            break;

        case BROADCAST:
        {
            for (int i = 0; i < Sockets.size(); ++i){
                Sockets[i]->write(Data);
                //Sockets[i]->waitForBytesWritten();
            }
            break;
        }
        case BROADCAST_IGNORE_SENDER:
        {
            for (int i = 0; i < Sockets.size(); ++i){
                if(socket == Sockets[i])
                    continue;
                Sockets[i]->write(Data);
                //Sockets[i]->waitForBytesWritten();
            }
        }
        default:
            break;

    }
}

void Server::SendToClient(bool flag, QTcpSocket* sender, TypeOfBroadcast typeBroadcast, quint16 messageType){
    Data.clear();
    QDataStream out(&Data, QIODevice::WriteOnly);
    QString loginSender = userLogin[sender];
    out.setVersion(QDataStream::Qt_DefaultCompiledVersion);

    switch(messageType){
        case MessageTypeLogin:
            break;

        case MessageTypePassword:
            break;

        case MessageTypePlainText:
            out << quint16(0) << messageType << loginSender << QTime::currentTime() << flag;
            break;

        case MessageTypeIsAuthDone:
            out << quint16(0) << messageType << QTime::currentTime() << flag;
            break;

        case MessageTypeRegisterUser:
            out << quint16(0) << messageType << QTime::currentTime() << flag;
            break;
        case MessageTypeRemoveUser:
            out << quint16(0) << messageType << loginSender << QTime::currentTime();
            break;
        case MessageTypeAddUser:
            out << quint16(0) << messageType << loginSender << QTime::currentTime();
            break;
    }
    out.device() -> seek(0);
    out << quint16(Data.size() - sizeof(quint16));

    switch(typeBroadcast){
        case PRIVATE:
            socket->write(Data);
            socket->waitForBytesWritten();
            break;

        case BROADCAST:
        {
            for (int i = 0; i < Sockets.size(); ++i){
                Sockets[i]->write(Data);
                Sockets[i]->waitForBytesWritten();
            }
            break;
        }
        case BROADCAST_IGNORE_SENDER:
        {
            for (int i = 0; i < Sockets.size(); ++i){
                if(socket == Sockets[i])
                    continue;
                Sockets[i]->write(Data);
                Sockets[i]->waitForBytesWritten();
            }
        }
        default:
            break;

    }
}

bool Server::connectDB(){
    mw_db = QSqlDatabase::addDatabase("QSQLITE");
        mw_db.setDatabaseName("authorisation");
        if(!mw_db.open())
        {
            qDebug() << "Cannot open database: " << mw_db.lastError();
            return false;
        }
        return true;
}

bool Server::registerUser(QTcpSocket* socket)
{
    QSqlQuery query;
    QSqlRecord rec;
    QString str_t = "SELECT COUNT(*) "
                    "FROM userlist;";
    db_input = str_t;
    QString _userName = userLogin[socket];
    QString _userPass = userPassword[socket];

    if(!query.exec(db_input))
    {
        qDebug() << "Unable to get number " << query.lastError() << " : " << query.lastQuery();
        return false;
    }
    else
    {
        query.next();
        rec = query.record();
        _userCount = rec.value(0).toInt();
    }

    _userCount++;
    str_t = "INSERT INTO userlist(number, name, pass) "
            "VALUES(%1, '%2', '%3');";
    db_input = str_t .arg(_userCount)
                     .arg(_userName)
                     .arg(_userPass);

    if(!query.exec(db_input))
    {
        qDebug() << "Unable to insert data"  << query.lastError() << " : " << query.lastQuery();
        return false;
    }
    return true;
}

