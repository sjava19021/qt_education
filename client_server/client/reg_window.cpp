#include "reg_window.h"
#include "ui_reg_window.h"

reg_window::reg_window(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::reg_window)
{
    ui->setupUi(this);
}

reg_window::~reg_window()
{
    delete ui;
}

void reg_window::on_nameLineEdit_textEdited(const QString &arg1)
{
    reg_window::_userName = arg1;
}


void reg_window::on_passwordLineEdit_textEdited(const QString &arg1)
{
    reg_window::_userPass = arg1;
}


void reg_window::on_confirmLineEdit_textEdited(const QString &arg1)
{
    reg_window::_confirmation = arg1;
}


void reg_window::on_registerPushButton_clicked()
{
    emit register_button_clicked2();
}

QString reg_window::getName(){
    return _userName;
}

QString reg_window::getPass(){
    return _userPass;
}

bool reg_window::checkPass(){
    return(_confirmation == _userPass);
}
