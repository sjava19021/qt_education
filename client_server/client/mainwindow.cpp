#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "auth_widow.h"
#include "reg_window.h"
#include <QDebug>

constexpr quint16 MessageTypeLogin = quint16(0);
constexpr quint16 MessageTypePassword = quint16(1);
constexpr quint16 MessageTypePlainText = quint16(2);
constexpr quint16 MessageTypeIsAuthDone = quint16(3);
constexpr quint16 MessageTypeRegisgetUser = quint16(4);
constexpr quint16 MessageTypeAddUser = quint16(5);
constexpr quint16 MessageTypeRemoveUser = quint16(6);
constexpr quint16 MessageTypePrivateMessage = quint16(7);

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    socket = new QTcpSocket(this);
    socket ->connectToHost("127.0.0.1", 2323);
    ui->comboBoxMailTo->addItem("public", QString("public"));

    connect(socket, &QTcpSocket::readyRead, this, &MainWindow::slotReadyRead);
    connect(socket, &QTcpSocket::disconnected, socket, &QTcpSocket::deleteLater);
    connect(socket, &QTcpSocket::connected, this, &MainWindow::socketConnected);
    connect(&ui_Auth, SIGNAL(login_button_clicked()), this, SLOT(authorizeUser()));
    connect(&ui_Auth, SIGNAL(destroyed()), this, SLOT(show()));
    connect(&ui_Auth, SIGNAL(register_button_clicked()), this, SLOT(registerWindowShow()));
    connect(&ui_Reg, SIGNAL(register_button_clicked2()), this, SLOT(registerUser()));
    connect(&ui_Reg,SIGNAL(destroyed()), &ui_Auth, SLOT(show()));

    //hазмер блока сообщения
    nextBlockSize = 0;

    //connectDB
    mw_db = QSqlDatabase::addDatabase("QSQLITE");
    mw_db.setDatabaseName("history");
    if(!mw_db.open())
    {
        qDebug() << "Cannot open database: " << mw_db.lastError();
    }

    QSqlQuery query;
    db_input = "CREATE TABLE history ( "
               "number INTEGER PRIMARY KEY NOT NULL,"
               "name VARCHAR(20), "
               "time VARCHAR(20), "
               "message VARCHAR(30)); ";

    if(!query.exec(db_input)){
        qDebug() << "Unable to create a table" << query.lastError();
    }

    //read db
    QString str_t = "SELECT * "
                    "FROM history;";
    while (query.next()) {
         QString name = query.value(0).toString();
         QString time = query.value(1).toString();
         QString message = query.value(2).toString();
         ui->textBrowser->append("[" + time  + "] " + name + ": " + message);
         //qDebug() << "[" + time  + "] " + name + ": " + message << "<<== from db";
   }
}

MainWindow::~MainWindow()
{
    if(socket->isOpen())
        socket->close();
    delete ui;
}



void MainWindow::SendToServer(QString str, quint16 messageType){
    Data.clear();
    QDataStream out(&Data, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_DefaultCompiledVersion);
    switch(messageType){
        case MessageTypeLogin:
            out << quint16(0) << messageType << QTime::currentTime() << _login;
            break;
        case MessageTypePassword:
            out << quint16(0) << messageType << QTime::currentTime() << _password;
            break;
        case MessageTypePlainText:
            out << quint16(0) << messageType << QTime::currentTime() << str;
            ui->lineEditMessage->clear();
            break;
        case MessageTypeIsAuthDone:
            out << quint16(0) << messageType << QTime::currentTime() << true;
            break;
        case MessageTypeRegisgetUser:
            out << quint16(0) << messageType << QTime::currentTime() << true;
            break;
        case MessageTypeRemoveUser:
            break;
        case MessageTypeAddUser:
            break;
    //case MessageTypePrivateMessage:

    }
    out.device() -> seek(0);
    out << quint16(Data.size() - sizeof(quint16));
    socket->write(Data);
    qDebug() << quint16(0) << messageType << QTime::currentTime() << str;
}


void MainWindow::slotReadyRead()
{
    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_DefaultCompiledVersion);
    if(in.status() == QDataStream::Ok){
        for(;;){
            if(nextBlockSize == 0){
                if(socket->bytesAvailable() < 2)
                    break;
                else
                    in >> nextBlockSize;
            }
            if(socket->bytesAvailable() < nextBlockSize){
                break;
            }
            quint16 messageType;
            QString str, login;
            QTime time;
            bool isDone;
            in >> messageType;
            switch (messageType){
                case MessageTypeLogin:
                    break;
                case MessageTypePassword:
                    break;
                case MessageTypePlainText:
                {
                    in >> login >> time >> str;
                    if(str.contains("-->CONNECTED TO SERVER!<--")){
                        ui->comboBoxMailTo->addItem(login, login);
                        ui->comboBoxMailTo->update();
                    } else if(str.contains("-->DISCONNECTED FROM SERVER!<--")){
                        int idx = ui->comboBoxMailTo->findText(login);
                        ui->comboBoxMailTo->removeItem(idx);
                        ui->comboBoxMailTo->update();
                    }
                    ui->textBrowser->append("[" + time.toString()  + "] " + login + ": " + str);
                    break;
                    //Insert into history
                    /*
                        int count = 0;
                        QSqlQuery query;
                        QSqlRecord rec;
                        QString str_t = "SELECT COUNT(*) "
                                        "FROM history;";
                        db_input = str_t;
                        QString _userName = _login;

                        if(!query.exec(db_input))
                        {
                            qDebug() << "Unable to get number " << query.lastError() << " : " << query.lastQuery();
                            return;
                        }
                        else
                        {
                            query.next();
                            rec = query.record();
                            count = rec.value(0).toInt();
                            qDebug() << count;
                        }

                        count++;

                        str_t = "INSERT INTO history(number, name, time, message) "
                                "VALUES(%1, '%2', '%3', '%4');";
                        db_input = str_t .arg(count)
                                         .arg(_userName)
                                         .arg(time.toString())
                                         .arg(str);

                        if(!query.exec(db_input))
                        {
                            qDebug() << "Unable to insert data"  << query.lastError() << " : " << query.lastQuery();
                        }*/
                    }
                    break;
                case MessageTypeIsAuthDone:
                    in >> time >> isDone;
                    if(isDone){
                        ui_Auth.hide();
                        this->show();
                    } else{
                        QMessageBox::warning(this, "Ошибка", "Неправильный логин и/или пароль");
                    }
                    break;

                case MessageTypeRegisgetUser:
                    in >> time >> isDone;
                    if(isDone){
                        ui_Reg.hide();
                        ui_Auth.show();
                    } else{
                        break;
                    }
                    break;

                case MessageTypeRemoveUser:
                {
                    in >> login >> time;
                    qDebug() << login << " removed";
                    int idx = ui->comboBoxMailTo->findText(login);
                    ui->comboBoxMailTo->removeItem(idx);
                    ui->comboBoxMailTo->update();
                }
                    break;

                case MessageTypeAddUser:
                    in >> login >> time;
                    ui->comboBoxMailTo->addItem(login, login);
                    ui->comboBoxMailTo->update();
                    break;

            }
            nextBlockSize = 0;
            break;
        }
    }
    else
        ui->textBrowser->append("read error");
}

void MainWindow::socketConnected(){
    //ui->textBrowser->append("connected to server!");
}

void MainWindow::socketError(int e){
    ui->textBrowser->append( tr("Error number %1 occurred\n").arg(e) );
}

void MainWindow::authorizeUser(){
    _login = ui_Auth.getLogin();
    _password = ui_Auth.getPass();

    SendToServer(_login, MessageTypeLogin);
    SendToServer(_password, MessageTypePassword);
    SendToServer("", MessageTypeIsAuthDone);
}

void MainWindow::registerWindowShow(){
    ui_Auth.hide();
    ui_Reg.show();
}

void MainWindow::registerUser()
{
    if(ui_Reg.checkPass())
    {
        //socket ->connectToHost("127.0.0.1", 2323);
        _login = ui_Reg.getName();
        _password = ui_Reg.getPass();
        SendToServer(_login, MessageTypeLogin);
        SendToServer(_password, MessageTypePassword);
        SendToServer("", MessageTypeRegisgetUser);
        ui_Reg.hide();
        ui_Auth.show();
    }
    else
    {
        QMessageBox::warning(this, "Ошибка", "Пароли не совпадают!");
    }
}

void MainWindow::display(){
    ui_Auth.show();
}


void MainWindow::on_pushButtonSend_clicked()
{
    //const int mailTo = ui->comboBoxMailTo->currentIndex();
    QString mailTo = ui->comboBoxMailTo->currentText();
    if(mailTo == QString("public")){
        SendToServer(ui->lineEditMessage->text(), MessageTypePlainText);
    }
    else{
        SendToServer("PRIVATE_" + mailTo + ":" + ui->lineEditMessage->text(), MessageTypePlainText);
    }
}


void MainWindow::on_lineEditMessage_returnPressed()
{
    SendToServer(ui->lineEditMessage->text(), MessageTypePlainText);
}


void MainWindow::on_comboBox_currentIndexChanged(const QString &arg1)
{
    return;
}

