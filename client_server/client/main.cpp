#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.setStyleSheet("QMainWindow {background: rgb(0, 120, 255);}");

    /*if(!w.connectDB()){
        exit(1);
    }*/
    w.display();

    return a.exec();
}
