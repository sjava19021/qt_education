#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAbstractSocket>
#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QHostAddress>
#include <QMessageBox>
#include <QMetaType>
#include <QString>
#include <QMessageBox>
#include <QStandardPaths>
#include <QTcpSocket>
#include <QTime>

#include <QtSql/QtSql>
#include "auth_widow.h"
#include "reg_window.h"

constexpr int maxLengthLogin = 30;
constexpr int maxLengthPassword = 30;
constexpr int maxLengthMessage = 100;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void display();
    bool connectDB();

private slots:
    void on_pushButtonSend_clicked();
    void on_lineEditMessage_returnPressed();

    void on_comboBox_currentIndexChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    QTcpSocket *socket;
    QByteArray Data;
    void SendToServer(QString str, quint16 messageType);
    quint16 nextBlockSize;
    QString _login;
    QString _password;

    auth_widow ui_Auth;
    reg_window ui_Reg;

    QString db_input;
    QSqlDatabase mw_db;

public slots:
    void slotReadyRead();
    void socketConnected();
    void socketError(int);

    void authorizeUser();
    void registerWindowShow();
    void registerUser();
};
#endif // MAINWINDOW_H
