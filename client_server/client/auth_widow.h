#ifndef AUTH_WIDOW_H
#define AUTH_WIDOW_H

#include <QWidget>
#include <QMessageBox>

namespace Ui {
class auth_widow;
}

class auth_widow : public QWidget
{
    Q_OBJECT

public:
    explicit auth_widow(QWidget *parent = nullptr);
    ~auth_widow();
    QString getLogin();
    QString getPass();

signals:
    void login_button_clicked();
    void register_button_clicked();

private slots:
    void on_lineEdit_textEdited(const QString &arg1);
    void on_lineEdit_2_textEdited(const QString &arg1);
    void on_loginPushButton_clicked();
    void on_registerPushButton_2_clicked();

private:
    Ui::auth_widow *ui;
    QString _userName;
    QString _userPass;
};

#endif // AUTH_WIDOW_H
