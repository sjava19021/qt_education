#include "auth_widow.h"
#include "ui_auth_widow.h"

constexpr int maxLengthLogin = 30;
constexpr int maxLengthPassword = 30;
constexpr int maxLengthMessage = 100;

auth_widow::auth_widow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::auth_widow)
{
    ui->setupUi(this);
}

auth_widow::~auth_widow()
{
    delete ui;
}

void auth_widow::on_lineEdit_textEdited(const QString &arg1)
{
    if(arg1.length() <= maxLengthLogin)
        auth_widow::_userName = arg1;
    else
        QMessageBox::warning(this, "Ошибка", "Максимальная длина логина 30 символов");
}

void auth_widow::on_lineEdit_2_textEdited(const QString &arg1)
{
    if(arg1.length() <= maxLengthPassword)
        auth_widow::_userPass = arg1;
    else
         QMessageBox::warning(this, "Ошибка", "Максимальная длина пароля 30 символов");
}


void auth_widow::on_loginPushButton_clicked()
{
    emit login_button_clicked();
}


void auth_widow::on_registerPushButton_2_clicked()
{
    emit register_button_clicked();
}

QString auth_widow::getLogin(){
    return auth_widow::_userName;
}

QString auth_widow::getPass(){
    return auth_widow::_userPass;
}

